terraform {
  required_providers {
    aws = {
      source  = "hashicorp/aws"
      version = "~> 4.0"
    }
  }
}

########################## Configure the AWS Provider ##################
provider "aws" {
  profile = var.profile
  #  access_key = var.AWS_ACCESS_KEY
  #  secret_key = var.AWS_SECRET_KEY
  shared_credentials_file = "~/.aws/credentials"
  region                  = var.AWS_REGION
}

######################### Create a VPC ################################
resource "aws_vpc" "example" {
  cidr_block = var.CIDR_BLOCK
  tags = {
    Name = "${var.env_prefix}-vpc"
  }
}
##################### create a public subnet1 #####################################
resource "aws_subnet" "public_subnet_1" {
  vpc_id            = aws_vpc.example.id
  cidr_block        = var.public_subnet_cidr_block[0]
  availability_zone = var.avail_zone[0]
  tags = {
    Name = "${var.env_prefix}-public-subnet1"
  }
}
##################### create a public subnet2 #####################################
resource "aws_subnet" "public_subnet_2" {
  vpc_id            = aws_vpc.example.id
  cidr_block        = var.public_subnet_cidr_block[1]
  availability_zone = var.avail_zone[1]
  tags = {
    Name = "${var.env_prefix}-public-subnet2"
  }
}
##################### create a private subnet1 #####################################
resource "aws_subnet" "private_subnet_1" {
  vpc_id            = aws_vpc.example.id
  cidr_block        = var.private_subnet_cidr_block[0]
  availability_zone = var.avail_zone[0]
  tags = {
    Name = "${var.env_prefix}-private-subnet1"
  }
}
##################### create a private subnet2 #####################################
resource "aws_subnet" "private_subnet_2" {
  vpc_id            = aws_vpc.example.id
  cidr_block        = var.private_subnet_cidr_block[1]
  availability_zone = var.avail_zone[1]
  tags = {
    Name = "${var.env_prefix}-private-subnet2"
  }
}
##################### create a private subnet3 #####################################
resource "aws_subnet" "private_subnet_3" {
  vpc_id            = aws_vpc.example.id
  cidr_block        = var.private_subnet_cidr_block[2]
  availability_zone = var.avail_zone[0]
  tags = {
    Name = "${var.env_prefix}-private-subnet3"
  }
}
##################### create a private subnet4 #####################################
resource "aws_subnet" "private_subnet_4" {
  vpc_id            = aws_vpc.example.id
  cidr_block        = var.private_subnet_cidr_block[3]
  availability_zone = var.avail_zone[1]
  tags = {
    Name = "${var.env_prefix}-private-subnet4"
  }
}
####################### Create internet Getway #############################
resource "aws_internet_gateway" "my-igw" {
  vpc_id = aws_vpc.example.id
  tags = {
    Name = "${var.env_prefix}-igw"
  }
}

######################## Create public route table ########################################
resource "aws_route_table" "public-rtb" {
  vpc_id = aws_vpc.example.id
  route {
    cidr_block = "0.0.0.0/0"
    gateway_id = aws_internet_gateway.my-igw.id
  }
  tags = {
    Name = "${var.env_prefix}-public-route-table"
  }
}
######################## Create private route table ########################################
resource "aws_route_table" "private-rtb" {
  vpc_id = aws_vpc.example.id
  route {
    cidr_block = "0.0.0.0/0"
    gateway_id = aws_internet_gateway.my-igw.id
  }
  tags = {
    Name = "${var.env_prefix}-private-route-table"
  }
}
####################### Associate route table to private subnet #######################
resource "aws_route_table_association" "private-subnets-1-assoc" {
  # count          = length(var.private_subnet_cidr_block_[*].id)
  # subnet_id      = element(var.private_subnet_cidr_block_[*].id, count.index)
  subnet_id      = aws_subnet.private_subnet_1.id
  route_table_id = aws_route_table.private-rtb.id
}
####################### Associate route table to private subnet #######################
resource "aws_route_table_association" "private-subnets-2-assoc" {
  # count          = length(var.private_subnet_cidr_block_[*].id)
  # subnet_id      = element(var.private_subnet_cidr_block_[*].id, count.index)
  subnet_id      = aws_subnet.private_subnet_2.id
  route_table_id = aws_route_table.private-rtb.id
}
####################### Associate route table to private subnet #######################
resource "aws_route_table_association" "private-subnets-3-assoc" {
  # count          = length(var.private_subnet_cidr_block_[*].id)
  # subnet_id      = element(var.private_subnet_cidr_block_[*].id, count.index)
  subnet_id      = aws_subnet.private_subnet_3.id
  route_table_id = aws_route_table.private-rtb.id
}
####################### Associate route table to private subnet #######################
resource "aws_route_table_association" "private-subnets-4-assoc" {
  # count          = length(var.private_subnet_cidr_block_[*].id)
  # subnet_id      = element(var.private_subnet_cidr_block_[*].id, count.index)
  subnet_id      = aws_subnet.private_subnet_4.id
  route_table_id = aws_route_table.private-rtb.id
}
###################### Associate route table to subnet ##############################
resource "aws_route_table_association" "rtb_association-1" {
  #  count          = length(var.public_subnet_cidr_block_[*].id)
  #  subnet_id      = element(var.public_subnet_cidr_block_[*].id, count.index)
  subnet_id      = aws_subnet.public_subnet_1.id
  route_table_id = aws_route_table.public-rtb.id
}
###################### Associate route table to subnet ##############################
resource "aws_route_table_association" "rtb_association-2" {
  #  count          = length(var.public_subnet_cidr_block_[*].id)
  #  subnet_id      = element(var.public_subnet_cidr_block_[*].id, count.index)
  subnet_id      = aws_subnet.public_subnet_2.id
  route_table_id = aws_route_table.public-rtb.id
}
######################### Search AWS AMI ######################################
data "aws_ami" "ubuntu" {
  most_recent = true
  owners      = ["amazon"]

  filter {
    name   = "name"
    values = ["ubuntu/images/hvm-ssd/ubuntu-jammy-22.04-amd64-server-*"]
  }

  filter {
    name   = "virtualization-type"
    values = ["hvm"]
  }
}
############### Create ssh key pair ############################
resource "aws_key_pair" "ssh-key" {
  key_name   = "mykey"
  public_key = file(var.ssh_key)
}
######################## Create a security Group for Jenkins ############################
resource "aws_security_group" "jenkins-sg" {
  name   = "jenkins-sg"
  vpc_id = aws_vpc.example.id

  ingress {
    from_port   = 22
    to_port     = 22
    protocol    = "tcp"
    cidr_blocks = [var.my_ip]
  }

  ingress {
    from_port   = 8080
    to_port     = 8080
    protocol    = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }
  egress {
    from_port       = 0
    to_port         = 0
    protocol        = "-1"
    cidr_blocks     = ["0.0.0.0/0"]
    prefix_list_ids = []
  }

  tags = {
    Name = "${var.env_prefix}jenkins-sg"
  }
}
####################### Security group nexus ###################
resource "aws_security_group" "nexus-sg" {
  name   = "nexus-sg"
  vpc_id = aws_vpc.example.id

  ingress {
    from_port   = 22
    to_port     = 22
    protocol    = "tcp"
    cidr_blocks = [var.my_ip]
  }
  ingress {
    from_port   = 8081
    to_port     = 8081
    protocol    = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }
  egress {
    from_port       = 0
    to_port         = 0
    protocol        = "-1"
    cidr_blocks     = ["0.0.0.0/0"]
    prefix_list_ids = []
  }

  tags = {
    Name = "${var.env_prefix}nexus-sg"
  }
}
####################### Security group artifactory ###################
resource "aws_security_group" "artifactory-sg" {
  name   = "artifactory-sg"
  vpc_id = aws_vpc.example.id

  ingress {
    from_port   = 22
    to_port     = 22
    protocol    = "tcp"
    cidr_blocks = [var.my_ip]
  }
  ingress {
    from_port   = 8081
    to_port     = 8081
    protocol    = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }
   ingress {
    from_port   = 8082
    to_port     = 8082
    protocol    = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }
  egress {
    from_port       = 0
    to_port         = 0
    protocol        = "-1"
    cidr_blocks     = ["0.0.0.0/0"]
    prefix_list_ids = []
  }

  tags = {
    Name = "${var.env_prefix}artifactory-sg"
  }
}
####################### Security group sonar ###################
resource "aws_security_group" "sonar-sg" {
  name   = "sonar-sg"
  vpc_id = aws_vpc.example.id

  ingress {
    from_port   = 22
    to_port     = 22
    protocol    = "tcp"
    cidr_blocks = [var.my_ip]
  }
  ingress {
    from_port   = 9000
    to_port     = 9000
    protocol    = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }
  ingress {
    from_port   = 80
    to_port     = 80
    protocol    = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }
  egress {
    from_port       = 0
    to_port         = 0
    protocol        = "-1"
    cidr_blocks     = ["0.0.0.0/0"]
    prefix_list_ids = []
  }

  tags = {
    Name = "${var.env_prefix}sonar-sg"
  }
}
##################### Security group Postgresql #######################
resource "aws_security_group" "postgres-sg" {
  name   = "postgres-sg"
  vpc_id = aws_vpc.example.id

  ingress {
    from_port   = 22
    to_port     = 22
    protocol    = "tcp"
    cidr_blocks = [var.my_ip]
  }
  ingress {
    from_port   = 5432
    to_port     = 5432
    protocol    = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }
  egress {
    from_port       = 0
    to_port         = 0
    protocol        = "-1"
    cidr_blocks     = ["0.0.0.0/0"]
    prefix_list_ids = []
  }

  tags = {
    Name = "${var.env_prefix}postgres-sg"
  }
}

###################### Create EC2 Jenkins #################################
resource "aws_instance" "jenkins-server" {
  ami                         = data.aws_ami.ubuntu.id
  instance_type               = var.instance_type
  key_name                    = aws_key_pair.ssh-key.key_name
  associate_public_ip_address = true
  subnet_id                   = aws_subnet.public_subnet_1.id
  vpc_security_group_ids      = [aws_security_group.jenkins-sg.id]
  availability_zone           = var.avail_zone[0]

  tags = {
    Name = "${var.env_prefix}-jenkins-server"
  }
  provisioner "local-exec" {
    command = <<EOF
aws --profile ${var.profile} ec2 wait instance-status-ok --region ${var.AWS_REGION} --instance-ids ${self.id}
ansible-playbook --extra-vars 'passed_in_hosts=${aws_instance.jenkins-server.public_ip}' ansible_templates/jenkins-ansible.yaml
EOF
  }

}
###################### Create EC2 nexus #################################
resource "aws_instance" "nexus-server" {
  ami                         = data.aws_ami.ubuntu.id
  instance_type               = var.instance_type
  key_name                    = aws_key_pair.ssh-key.key_name
  associate_public_ip_address = true
  subnet_id                   = aws_subnet.public_subnet_2.id
  vpc_security_group_ids      = [aws_security_group.nexus-sg.id]
  availability_zone           = var.avail_zone[1]

  tags = {
    Name = "${var.env_prefix}-nexus-server"
  }
  provisioner "local-exec" {
    command = <<EOF
aws --profile ${var.profile} ec2 wait instance-status-ok --region ${var.AWS_REGION} --instance-ids ${self.id}
ansible-playbook --extra-vars 'passed_in_hosts=${aws_instance.nexus-server.public_ip}' ansible_templates/nexus-ansible.yaml
EOF
  }

}
###################### Create EC2 jfrog #################################
resource "aws_instance" "jfrog-server" {
  ami                         = data.aws_ami.ubuntu.id
  instance_type               = var.instance_type
  key_name                    = aws_key_pair.ssh-key.key_name
  associate_public_ip_address = true
  subnet_id                   = aws_subnet.public_subnet_1.id
  vpc_security_group_ids      = [aws_security_group.artifactory-sg.id]
  availability_zone           = var.avail_zone[0]
  
  tags = {
    Name = "${var.env_prefix}-jfrog-server"
  }
  provisioner "local-exec" {
    command = <<EOF
aws --profile ${var.profile} ec2 wait instance-status-ok --region ${var.AWS_REGION} --instance-ids ${self.id}
ansible-playbook --extra-vars 'passed_in_hosts=${aws_instance.jfrog-server.public_ip}' ansible_templates/jfrog-ansible.yaml
EOF
 }
 
}
###################### Create EC2 sonar #################################
resource "aws_instance" "sonar-server" {
  ami                         = data.aws_ami.ubuntu.id
  instance_type               = var.instance_type
  key_name                    = aws_key_pair.ssh-key.key_name
  associate_public_ip_address = true
  subnet_id                   = aws_subnet.private_subnet_2.id
  vpc_security_group_ids      = [aws_security_group.sonar-sg.id]
  availability_zone           = var.avail_zone[1]
  user_data = file("sonar.sh")
  
  tags = {
    Name = "${var.env_prefix}-sonar-server"
  }

}
###################### Create EC2 Postgresql  #################################
resource "aws_instance" "postgres-server" {
  ami                         = data.aws_ami.ubuntu.id
  instance_type               = var.instance_type
  key_name                    = aws_key_pair.ssh-key.key_name
  associate_public_ip_address = true
  subnet_id                   = aws_subnet.private_subnet_1.id
  vpc_security_group_ids      = [aws_security_group.postgres-sg.id]
  availability_zone           = var.avail_zone[0]

  tags = {
    Name = "${var.env_prefix}-postgres-server"
  }

}

