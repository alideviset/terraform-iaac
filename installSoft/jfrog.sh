#!/bin/bash
sudo apt update
wget -O jfrog-artifactory-pro.tar.gz "https://releases.jfrog.io/artifactory/artifactory-pro/org/artifactory/pro/jfrog-artifactory-pro/[RELEASE]/jfrog-artifactory-pro-[RELEASE]-linux.tar.gz"
sudo tar -xvzf jfrog-artifactory-pro.tar.gz
sudo rm -rf jfrog-artifactory-pro.tar.gz
./artifactory-pro*/app/bin/artifactoryctl start
