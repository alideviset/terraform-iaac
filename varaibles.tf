variable "AWS_ACCESS_KEY" {}
variable "AWS_SECRET_KEY" {}
variable "AWS_REGION" {}
variable "CIDR_BLOCK" {}
variable "env_prefix" {}
variable "public_subnet_cidr_block" {}
variable "private_subnet_cidr_block" {}
variable "avail_zone" {}
variable "instance_type" {}
variable "my_ip" {}
variable "ssh_key" {}
variable "priv_ssh_key" {}
variable "profile" {
  type    = string
  default = "default"
}

