output "ami_id" {
  value = data.aws_ami.ubuntu.id
}
output "jenkins-ip" {
  value = aws_instance.jenkins-server.public_ip
}
output "jfrog-ip" {
  value = aws_instance.jfrog-server.public_ip
}
output "sonar-ip" {
  value = aws_instance.sonar-server.public_ip
}
output "nexus-ip" {
  value = aws_instance.nexus-server.public_ip
}
output "Postgresql-ip" {
  value = aws_instance.postgres-server.public_ip
}


