#!/bin/bash
sudo apt install python3-pip -y
pip3 install boto3 --user
python3 get-pip.py --user
python3 -m pip install --user ansible
sudo apt update
sudo apt install software-properties-common
sudo add-apt-repository --yes --update ppa:ansible/ansible
sudo apt install ansible -y

